package com.polovtseva.news.dao.impl;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.polovtseva.news.dao.CommentDAO;
import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.util.CommentDataLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

/**
 * Test for CommentDAOImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class CommentDAOImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/testCreateCommentResult.xml";


    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private CommentDataLoader commentDataLoader;


    @Test
    @DatabaseSetup(value = "classpath:test/testCreateCommentResult.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/testCreateCommentResult.xml"}, type = DatabaseOperation.DELETE)
    public void findOneTest() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            Comment foundComment = commentDAO.findOne(comment.getCommentId());
            Assert.assertEquals(comment, foundComment);
        }
    }

    @Test
    @ExpectedDatabase(value = "classpath:test/testDeleteCommentResult.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/testDeleteCommentResult.xml"}, type = DatabaseOperation.DELETE)
    public void deleteTest() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            commentDAO.delete(comment);
        }
    }

    @Test
    @DatabaseSetup(value = "classpath:test/testCreateComment.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/testCreateCommentResult.xml"}, type = DatabaseOperation.DELETE)
    public void createTest() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            Comment createdComment = commentDAO.create(comment);
            Assert.assertEquals(comment, createdComment);
        }
    }


}
