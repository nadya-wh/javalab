package com.polovtseva.news.dao.impl;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.polovtseva.news.dao.NewsDAO;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.util.NewsDataLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Assert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Test for NewsDAOImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class NewsDAOImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";
    public static final String TEST_EDIT_SAMPLE_DATA_XML = "test/editNewsSampleData.xml";

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private NewsDataLoader newsDataLoader;

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindOne() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            News foundNews = newsDAO.findOne(news.getNewsId());
            Assert.assertTrue(foundNews.equals(news));
        }
    }


    @Test
    @DatabaseSetup(value = "classpath:test/testDeleteNews.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/testDeleteNewsResult.xml"}, type = DatabaseOperation.DELETE)
    @ExpectedDatabase(value = "classpath:test/testDeleteNewsResult.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDelete() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            newsDAO.delete(news);
        }
    }


    @Test
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testCreateNews() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        List<News> createdNews = new ArrayList<>(allNews.size());
        for (News news : allNews) {
            createdNews.add(newsDAO.create(news));
        }
        Assert.assertEquals(allNews, createdNews);
    }

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindAll() throws Exception {
        List<News> expected = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        List<News> actual = newsDAO.findAll();
        Assert.assertEquals(expected.size(), actual.size());
    }

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/editNewsSampleData.xml"}, type = DatabaseOperation.DELETE)
    @ExpectedDatabase(value = "classpath:test/editNewsSampleData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testEditNews() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        List<News> result = newsDataLoader.loadData(TEST_EDIT_SAMPLE_DATA_XML);
        for (int i = 0; i < allNews.size(); i++) {
            News news = allNews.get(i);
            news.setTitle(result.get(i).getTitle());
            news.setModificationDate(result.get(i).getModificationDate());
            news.setCreationDate(result.get(i).getCreationDate());
            news.setFullText(result.get(i).getFullText());
            news.setShortText(result.get(i).getShortText());

            newsDAO.update(news);
        }
    }

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/editNewsSampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testCountNews() throws Exception {
        List<News> expected = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        Assert.assertEquals(expected.size(), newsDAO.countNews().intValue());
    }
}
