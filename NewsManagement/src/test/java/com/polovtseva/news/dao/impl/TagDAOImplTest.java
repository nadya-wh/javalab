package com.polovtseva.news.dao.impl;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.util.TagDataLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

/**
 * Test for TagDAOImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class TagDAOImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";
    @Autowired
    private TagDAO tagDAO;


    @Autowired
    private TagDataLoader tagDataLoader;

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindAll() throws Exception {
        List<Tag> expected = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        List<Tag> actual = tagDAO.findAll();
        Assert.assertEquals(expected.size(), actual.size());
    }

    @Test
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testCreate() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            Tag createdTag = tagDAO.create(tag);
            Assert.assertEquals(createdTag, tag);
        }
    }

    /**
     * Test unique tag name.
     *
     * @throws Exception expected result.
     */
    @Test(expected = DAOException.class)
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testCreateNegative() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            System.out.println(tagDAO.create(tag));
        }
    }


    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindOne() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            Tag foundTag = tagDAO.findOne(tag.getTagId());
            Assert.assertEquals(foundTag, tag);
        }
    }


}
