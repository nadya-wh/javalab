package com.polovtseva.news.dao.impl;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import com.polovtseva.news.dao.AuthorDAO;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.util.AuthorDataLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

/**
 * Test for AuthorDAOImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class AuthorDAOImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";
    public static final String TEST_EDIT_SAMPLE_DATA_XML = "test/editAuthorSampleData.xml";

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private AuthorDataLoader authorDataLoader;

    @Test
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testCreate() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            Author created = authorDAO.create(author);
            Assert.assertEquals(created, author);
        }
    }

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindOne() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            Author found = authorDAO.findOne(author.getAuthorId());
            Assert.assertEquals(found.getAuthorId(), author.getAuthorId());
        }
    }


    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    @ExpectedDatabase(value = "classpath:test/editAuthorSampleData.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdate() throws Exception {
        List<Author> result = authorDataLoader.loadData(TEST_EDIT_SAMPLE_DATA_XML);
        for (Author author : result) {
            authorDAO.update(author);
        }
    }

    @Test
    @DatabaseSetup(value = "classpath:test/sampleData.xml", type = DatabaseOperation.CLEAN_INSERT)
    @DatabaseTearDown(value = {"classpath:test/sampleData.xml"}, type = DatabaseOperation.DELETE)
    public void testFindAll() throws Exception {
        List<Author> authors = authorDAO.findAll();
        List<Author> expected = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        Assert.assertEquals(authors.size(), expected.size());
    }



}
