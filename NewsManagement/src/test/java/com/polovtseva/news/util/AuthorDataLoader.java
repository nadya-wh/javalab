package com.polovtseva.news.util;

import com.polovtseva.news.entity.Author;
import com.polovtseva.news.util.exception.DataLoaderException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Author data loader.
 */
public class AuthorDataLoader implements DataLoader<Author> {

    @Override
    public List<Author> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            return parseDataSet(dataSet);
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Author> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Author> authors = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();
        DateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();
            if ("authors".equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long authorId = Long.parseLong(tableIterator.getTable().getValue(i, "author_id").toString());
                    String authorName = tableIterator.getTable().getValue(i, "author_name").toString();
                    Timestamp expired = new Timestamp(format.parse(tableIterator.getTable().getValue(i, "expired").toString()).getTime());
                    authors.add(new Author(authorId, authorName, expired));
                }
            }
        }
        return authors;
    }
}
