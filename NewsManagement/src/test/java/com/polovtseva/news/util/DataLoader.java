package com.polovtseva.news.util;

import com.polovtseva.news.util.exception.DataLoaderException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import java.util.List;

/**
 * Data loader.
 */
public interface DataLoader<T> {
    List<T> loadData(String file) throws DataLoaderException;

    default IDataSet getDataSet(String file) throws DataSetException {
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        builder.setColumnSensing(true);
        return builder.build(getClass().getClassLoader().getResourceAsStream(file));
    }
}
