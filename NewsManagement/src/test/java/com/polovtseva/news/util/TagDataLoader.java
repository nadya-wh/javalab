package com.polovtseva.news.util;

import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.util.exception.DataLoaderException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Tag data loader.
 */
public class TagDataLoader implements DataLoader<Tag> {

    public List<Tag> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            List<Tag> tags = parseDataSet(dataSet);
            return tags;
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Tag> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Tag> tags = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();
        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();
            if ("tags".equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long tagId = Long.parseLong(tableIterator.getTable().getValue(i, "tag_id").toString());
                    String tagName = tableIterator.getTable().getValue(i, "tag_name").toString();
                    tags.add(new Tag(tagId, tagName));
                }
            }
        }
        return tags;
    }
}
