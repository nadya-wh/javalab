package com.polovtseva.news.util;

import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.util.exception.DataLoaderException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Comment data loader.
 */
public class CommentDataLoader implements DataLoader<Comment> {
    @Override
    public List<Comment> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            return parseDataSet(dataSet);
        } catch (ParseException | DataSetException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<Comment> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<Comment> comments = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();
        DateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();
            if ("comments".equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long commentId = Long.parseLong(tableIterator.getTable().getValue(i, "comment_id").toString());
                    Long newsId = Long.parseLong(tableIterator.getTable().getValue(i, "news_id").toString());
                    String commentText = tableIterator.getTable().getValue(i, "comment_text").toString();
                    Timestamp creationDate = new Timestamp(format.parse(tableIterator.getTable().getValue(i, "creation_date").toString()).getTime());
                    comments.add(new Comment(commentId, commentText, creationDate, newsId));
                }
            }
        }
        return comments;
    }
}
