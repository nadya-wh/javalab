package com.polovtseva.news.util;

import com.polovtseva.news.entity.News;
import com.polovtseva.news.util.exception.DataLoaderException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * News data loader.
 */
public class NewsDataLoader implements DataLoader<News> {
    @Override
    public List<News> loadData(String file) throws DataLoaderException {
        try {
            IDataSet dataSet = getDataSet(file);
            return parseDataSet(dataSet);
        } catch (DataSetException | ParseException e) {
            throw new DataLoaderException(e);
        }
    }

    private List<News> parseDataSet(IDataSet dataSet) throws DataSetException, ParseException {
        List<News> allNews = new ArrayList<>();
        ITableIterator tableIterator = dataSet.iterator();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setLenient(false);
        while (tableIterator.next()) {
            ITableMetaData metadata = tableIterator.getTableMetaData();
            if ("news".equalsIgnoreCase(metadata.getTableName())) {
                for (int i = 0; i < tableIterator.getTable().getRowCount(); i++) {
                    Long newsId = Long.parseLong(tableIterator.getTable().getValue(i, "news_id").toString());
                    String title = tableIterator.getTable().getValue(i, "title").toString();
                    String shortText = tableIterator.getTable().getValue(i, "short_text").toString();
                    String fullText = tableIterator.getTable().getValue(i, "full_text").toString();
                    Timestamp creationDate = new Timestamp(format.parse(tableIterator.getTable().getValue(i, "creation_date").toString()).getTime());
                    Timestamp modificationDate = new Timestamp(format.parse(tableIterator.getTable().getValue(i, "modification_date").toString()).getTime());
                    allNews.add(new News(newsId, title, shortText, fullText, creationDate, modificationDate));
                }
            }
        }
        return allNews;
    }
}
