package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.AuthorDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.service.exception.ServiceException;
import com.polovtseva.news.util.AuthorDataLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test for AuthorServiceImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class AuthorServiceImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";

    @Mock
    private AuthorDAO authorDAO;
    @InjectMocks
    private AuthorServiceImpl authorService;

    @Autowired
    private AuthorDataLoader authorDataLoader;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = ServiceException.class)
    public void testCreateException() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            when(authorDAO.create(author)).thenThrow(new DAOException());
            authorService.create(author);
        }
    }

    @Test
    public void testCreate() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            when(authorDAO.create(author)).thenReturn(author);
            Assert.assertEquals(authorService.create(author), author);
        }
    }

    @Test
    public void testFindOne() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            when(authorDAO.findOne(author.getAuthorId())).thenReturn(author);
            Assert.assertEquals(authorService.findOne(author.getAuthorId()), author);
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<Author> expected = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        when(authorDAO.findAll()).thenReturn(expected);
        Assert.assertEquals(expected, authorService.findAll());

    }

    @Test
    public void testUpdatePositive() throws Exception {
        List<Author> authors = authorDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Author author : authors) {
            when(authorDAO.update(author)).thenReturn(true);
            Assert.assertTrue(authorService.update(author));
        }
    }

    @Test
    public void testUpdateNull() throws Exception {
        Assert.assertFalse(authorService.update(null));
    }

}
