package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.CommentDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.service.exception.ServiceException;
import com.polovtseva.news.util.CommentDataLoader;
import com.polovtseva.news.util.NewsDataLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test for CommentServiceImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class CommentServiceImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";

    @Mock
    private CommentDAO commentDAO;
    @InjectMocks
    private CommentServiceImpl commentService;

    @Autowired
    private CommentDataLoader commentDataLoader;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = ServiceException.class)
    public void testAddCommentNegative() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            when(commentDAO.create(comment)).thenThrow(new DAOException());
            comment.setCommentId(null);
            commentService.addComment(comment);
        }
    }

    @Test
    public void testAddCommentPositive() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            when(commentDAO.create(comment)).thenReturn(comment);
            Assert.assertEquals(comment, commentService.addComment(comment));
        }
    }

    @Test
    public void testDelete() throws Exception {
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Comment comment : comments) {
            when(commentDAO.delete(comment)).thenReturn(true);
            Assert.assertTrue(commentService.delete(comment).booleanValue());
        }
    }

    @Test
    public void testFindAllFor() throws Exception {
        List<News> allNews = new NewsDataLoader().loadData(TEST_SAMPLE_DATA_XML);
        List<Comment> comments = commentDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            when(commentDAO.findAllFor(news)).thenReturn(comments);
            Assert.assertEquals(comments, commentService.findAllFor(news));
        }
    }

}
