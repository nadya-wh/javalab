package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.exception.ServiceException;
import com.polovtseva.news.util.TagDataLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test for TagServiceImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class TagServiceImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";

    @Mock
    private TagDAO tagDAO;

    @InjectMocks
    private TagServiceImpl tagService;

    @Autowired
    private TagDataLoader tagDataLoader;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test

    public void testFindEntityById() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            when(tagDAO.findOne(tag.getTagName())).thenReturn(tag);
            assertEquals(tagService.findOne(tag.getTagName()), tag);
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        when(tagDAO.findAll()).thenReturn(tags);
        assertEquals(tagService.findAll().size(), tags.size());

    }

    @Test
    public void testCreate() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            when(tagDAO.create(tag)).thenReturn(tag);
            tag.setTagId(null);
            assertEquals(tagService.createTag(tag), tag);
        }
    }

    @Test(expected = ServiceException.class)
    public void testCreateWithException() throws Exception {
        List<Tag> tags = tagDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (Tag tag : tags) {
            when(tagDAO.create(tag)).thenThrow(new DAOException());
            tag.setTagId(null);
            tagService.createTag(tag);
        }
    }
}
