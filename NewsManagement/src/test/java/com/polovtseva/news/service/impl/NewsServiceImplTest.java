package com.polovtseva.news.service.impl;


import com.polovtseva.news.dao.NewsDAO;
import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.service.exception.ServiceException;
import com.polovtseva.news.util.AuthorDataLoader;
import com.polovtseva.news.util.NewsDataLoader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test for NewsServiceImpl.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DirtiesContextBeforeModesTestExecutionListener.class})
public class NewsServiceImplTest {

    public static final String TEST_SAMPLE_DATA_XML = "test/sampleData.xml";
    public static final String TEST_AUTHOR = "test/author.xml";

    @Mock
    private NewsDAO newsDAO;

    @Mock
    private TagDAO tagDAO;
    @InjectMocks
    private NewsServiceImpl newsService;


    @Autowired
    private NewsDataLoader newsDataLoader;

    @Autowired
    private AuthorDataLoader authorDataLoader;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            when(newsDAO.create(news)).thenReturn(news);
            Assert.assertEquals(newsService.addNews(news), news);
        }
    }

    @Test
    public void testDeleteNews() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            when(newsDAO.delete(news)).thenReturn(true);
            Assert.assertTrue(newsService.deleteNews(news));
        }
    }

    @Test
    public void testEditNews() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        for (News news : allNews) {
            when(newsDAO.update(news)).thenReturn(true);
            Assert.assertTrue(newsService.editNews(news));
        }
    }

    @Test(expected = ServiceException.class)
    public void testCreateTransactionalNegative() throws Exception {
        List<News> allNews = newsDataLoader.loadData(TEST_SAMPLE_DATA_XML);
        List<Author> authors = authorDataLoader.loadData(TEST_AUTHOR);
        if (!authors.isEmpty()) {
            for (News news : allNews) {
                news.setAuthor(authors.get(0));
                when(newsDAO.create(news)).thenThrow(new DAOException());
                Assert.assertEquals(newsService.addNews(news), news);
            }
        }
    }

}
