INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'The first news', 'Say hi', 'Hello, world!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'The second news', 'Say hi and welcome', 'Hello, world! Welcome!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'Titanic', 'Fatal crash', 'Bad news(', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'Something is going on', 'Say hi and welcome', 'Hello, world! Welcome!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'Hi, hi, hi-hi', 'Say hi and welcome', 'Hello, world! Welcome!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'No news is good news', 'Say hi and welcome', 'Hello, world! Welcome!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'Getting bored', 'Say hi and welcome', 'Hello, world! Welcome!', sysdate, sysdate);
INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, 'Have no imagination', 'Say hi and welcome', 'Sorry :(', sysdate, sysdate);

COMMIT;

INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Leo Tolstoy', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Joanne Rowling', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Ernest Hemingway', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Sheryl Strayed', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Steven Chbosky', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Fedor Dostoevsky', ADD_MONTHS(sysdate, 24));
INSERT INTO AUTHORS (author_id, author_name, expired) VALUES (AUTHOR_ID_SEQUENCE.nextval, 'Theodore Dreiser', ADD_MONTHS(sysdate, 24));

INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'best');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'sun');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'moon');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'fun');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'luck');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'worst');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'sunny');
INSERT INTO TAGS (tag_id, tag_name) VALUES (TAG_ID_SEQUENCE.nextval, 'cloudy');



COMMIT;


INSERT INTO news_author (news_id, author_id) VALUES (1, 1);
INSERT INTO news_author (news_id, author_id) VALUES (2, 1);
INSERT INTO news_author (news_id, author_id) VALUES (3, 1);
INSERT INTO news_author (news_id, author_id) VALUES (4, 1);
INSERT INTO news_author (news_id, author_id) VALUES (5, 1);

COMMIT;


INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);
INSERT INTO news_tag (news_id, tag_id) VALUES (1, 1);

INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 1, 'Hi to you too', sysdate);
INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 2, 'Good day', sysdate);
INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 3, 'Too sunny', sysdate);
INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 4, 'Too cloudy', sysdate);
INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 5, 'Too many letters', sysdate);
INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (COMMENT_ID_SEQUENCE.nextval, 6, 'Good news!', sysdate);

COMMIT;
