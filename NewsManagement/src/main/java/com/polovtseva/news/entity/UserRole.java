package com.polovtseva.news.entity;

/**
 * User role enum.
 */
public enum UserRole {
    ADMIN,
    USER,
    BLOCKED
}
