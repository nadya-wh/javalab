package com.polovtseva.news.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Author entity.
 */
public class Author implements Serializable {

    private Long authorId;
    private String authorName;
    private Date expired;

    public Author(Long authorId, String authorName, Date expired) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.expired = expired;
    }

    public Author(String authorName, Date expired) {
        this.authorName = authorName;
        this.expired = expired;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        return expired != null ? expired.getTime() == author.expired.getTime() : author.expired == null;

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", expired=" + expired +
                '}';
    }
}
