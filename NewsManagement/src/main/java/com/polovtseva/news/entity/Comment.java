package com.polovtseva.news.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Comment entity.
 */
public class Comment implements Serializable {
    private Long commentId;
    private String commentText;
    private Date creationDate;
    private Long newsId;

    public Comment(Long commentId, String commentText, Date creationDate, Long newsId) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Comment(String commentText, Date creationDate, Long newsId) {
        this.commentText = commentText;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (commentId != null ? !commentId.equals(comment.commentId) : comment.commentId != null) return false;
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
            return false;
        return newsId != null ? newsId.equals(comment.newsId) : comment.newsId == null;

    }

    @Override
    public int hashCode() {
        int result = commentId != null ? commentId.hashCode() : 0;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                ", newsId=" + newsId +
                '}';
    }
}
