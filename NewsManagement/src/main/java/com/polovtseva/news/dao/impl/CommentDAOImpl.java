package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.CommentDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.CommentMapper;
import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.entity.News;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 27.05.2016.
 */
public class CommentDAOImpl implements CommentDAO {

    private DataSource dataSource;
    private CommentMapper commentMapper;

    private static final String SELECT_ALL_FOR_NEWS = "SELECT comment_id, news_id, comment_text, creation_date " +
            "FROM comments WHERE news_id=?";


    private static final String SELECT_ONE_BY_ID = "SELECT comment_id, news_id, comment_text, creation_date " +
            "FROM comments WHERE comment_id=?";


    private static final String INSERT_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) " +
            "VALUES(COMMENT_ID_SEQUENCE.nextval, ?, ?, ?)";


    private static final String DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";


    private static final String SELECT_ALL = "SELECT comment_id, news_id, comment_text, creation_date " +
            "FROM comments";


    public CommentDAOImpl(DataSource dataSource, CommentMapper commentMapper) {
        this.dataSource = dataSource;
        this.commentMapper = commentMapper;
    }

    @Override
    public List<Comment> findAll() throws DAOException {
        return findAll(dataSource, SELECT_ALL, commentMapper);
    }

    @Override
    public List<Comment> findAllFor(News news) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FOR_NEWS)) {
                preparedStatement.setLong(1, news.getNewsId());

                ResultSet resultSet = preparedStatement.executeQuery();
                return commentMapper.takeAll(resultSet);

            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Comment findOne(Long id) throws DAOException {
        return findOne(id, dataSource, SELECT_ONE_BY_ID, commentMapper);
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return delete(id, DELETE_COMMENT, dataSource);
    }

    @Override
    public Boolean delete(Comment entity) throws DAOException {
        return delete(entity.getCommentId());
    }

    @Override
    public Comment create(Comment entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_COMMENT,
                    new String[]{"comment_id"})) {
                preparedStatement.setLong(1, entity.getNewsId());
                preparedStatement.setString(2, entity.getCommentText());
                preparedStatement.setDate(3, new java.sql.Date(entity.getCreationDate().getTime()));
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setCommentId(resultSet.getLong(1));
                    return entity;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean update(Comment entity) throws DAOException {
        return null;
    }


}