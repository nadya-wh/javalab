package com.polovtseva.news.dao;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.entity.News;

import java.util.List;

/**
 * Comment DAO interface.
 */
public interface CommentDAO extends GenericDAO<Comment> {

    /**
     * Find all for news.
     *
     * @param news the news
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<Comment> findAllFor(News news) throws DAOException;

}
