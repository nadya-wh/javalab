package com.polovtseva.news.dao;

import com.polovtseva.news.entity.Author;

/**
 * Author DAO interface.
 */
public interface AuthorDAO extends GenericDAO<Author> {
}
