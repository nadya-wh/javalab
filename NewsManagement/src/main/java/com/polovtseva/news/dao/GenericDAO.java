package com.polovtseva.news.dao;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.RowMapper;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Generic DAO, describes CRUD operations.
 */
public interface GenericDAO<T> {

    /**
     * Find all.
     *
     * @return the list
     * @throws DAOException the DAO exception.
     */
    List<T> findAll() throws DAOException;


    /**
     * Find one.
     *
     * @param id the id
     * @return the t
     * @throws DAOException the DAO exception
     */
    T findOne(Long id) throws DAOException;

    /**
     * Delete.
     *
     * @param id the id
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean delete(Long id) throws DAOException;

    /**
     * Delete.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean delete(T entity) throws DAOException;


    /**
     * Creates entity.
     *
     * @param entity the entity
     * @return the t
     * @throws DAOException the DAO exception
     */
    T create(T entity) throws DAOException;


    /**
     * Update.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean update(T entity) throws DAOException;


    /**
     * Delete.
     *
     * @param id the id
     * @param query the query
     * @param dataSource the data source
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    default Boolean delete(Long id, String query, DataSource dataSource) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                return preparedStatement.executeUpdate() > 0;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Finds one entity.
     *
     * @param id the id
     * @param dataSource the data source
     * @param query the query
     * @return the t
     * @throws DAOException the DAO exception
     */
    default T findOne(Long id, DataSource dataSource, String query, RowMapper<T> rowMapper) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return rowMapper.takeOne(resultSet);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Finds all entities.
     *
     * @param dataSource the data source
     * @param statement the statement
     * @return the list
     * @throws DAOException the DAO exception
     */
    default List<T> findAll(DataSource dataSource, String statement, RowMapper<T> rowMapper) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                return rowMapper.takeAll(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

}
