package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.UserDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.UserMapper;
import com.polovtseva.news.entity.User;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by User on 26.05.2016.
 */
public class UserDAOImpl implements UserDAO {

    private DataSource dataSource;
    private UserMapper userMapper;

    private static final String CHECK_USER_EXISTS = "SELECT user_id FROM users WHERE login=? AND password=?";

    private static final String INSERT_USER = "INSERT INTO users (user_name, login, password) VALUES " +
            "(?, ?, ?)";

    private static final String SELECT_ALL = "SELECT user_id, user_name, login, password FROM users";

    private static final String SELECT_ONE = "SELECT user_id, user_name, login, password FROM users " +
            "WHERE user_id=?";

    private static final String DELETE_ONE = "DELETE FROM users WHERE user_id=?";


    public UserDAOImpl(DataSource dataSource, UserMapper userMapper) {
        this.dataSource = dataSource;
        this.userMapper = userMapper;
    }

    @Override
    public Boolean authenticate(String login, String password) throws DAOException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CHECK_USER_EXISTS)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<User> findAll() throws DAOException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            return userMapper.takeAll(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public User findOne(Long id) throws DAOException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return userMapper.takeOne(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return delete(id, DELETE_ONE, dataSource);
    }

    @Override
    public Boolean delete(User entity) throws DAOException {
        return delete(entity.getUserId());
    }

    @Override
    public User create(User entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, new String[]{"user_id"})) {
                preparedStatement.setString(1, entity.getUserName());
                preparedStatement.setString(2, entity.getLogin());
                preparedStatement.setString(3, entity.getPassword());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setUserId(resultSet.getLong(1));
                    return entity;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean update(User entity) throws DAOException {
        return null;
    }

}