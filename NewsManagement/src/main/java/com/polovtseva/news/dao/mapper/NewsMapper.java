package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.News;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class NewsMapper implements RowMapper<News> {
    @Override
    public List<News> takeAll(ResultSet resultSet) throws DAOException {
        List<News> allNews = new ArrayList<>();
        try {
            while (resultSet.next()) {
                allNews.add(takeOne(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return allNews;
    }

    @Override
    public News takeOne(ResultSet resultSet) throws DAOException {
        try {
            Long newsId = resultSet.getLong("news_id");
            String title = resultSet.getString("title");
            String shortText = resultSet.getString("short_text");
            String fullText = resultSet.getString("full_text");
            Date creationDate = resultSet.getTimestamp("creation_date");
            Date modificationDate = resultSet.getTimestamp("modification_date");
            News news = new News(newsId, title, shortText, fullText, creationDate, modificationDate);
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
