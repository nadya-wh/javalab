package com.polovtseva.news.dao;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Tag;

/**
 * Tag DAO.
 */
public interface TagDAO extends GenericDAO<Tag> {

    /**
     * Finds tag by name.
     * @param tagName the tag name.
     * @return tag with specified name or null.
     * @throws DAOException if database error occurred.
     */
    Tag findOne(String tagName) throws DAOException;

    Boolean linkNewsTag(Long newsId, Long tagId) throws DAOException;
}
