package com.polovtseva.news.dao;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.User;

/**
 * User DAO.
 */
public interface UserDAO extends GenericDAO<User> {

    /**
     * Authenticate.
     *
     * @param login the login
     * @param password the password
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean authenticate(String login, String password) throws DAOException;
}
