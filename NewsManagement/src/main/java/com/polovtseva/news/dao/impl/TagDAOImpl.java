package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.TagMapper;
import com.polovtseva.news.entity.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
public class TagDAOImpl implements TagDAO {

    private DataSource dataSource;
    private TagMapper tagMapper;

    private static final String INSERT_TAG = "INSERT INTO TAGS (tag_id, tag_name) VALUES (tag_id_sequence.nextval, ?)";

    private static final String SELECT_TAG_BY_NAME = "SELECT tag_id, tag_name FROM TAGS WHERE " +
            "tag_name=?";

    private static final String SELECT_TAG_BY_ID = "SELECT tag_id, tag_name FROM tags WHERE " +
            "tag_id=?";

    private static final String SELECT_ALL = "SELECT tag_id, tag_name FROM tags";

    private static final String INSERT_NEWS_TAG = "INSERT INTO news_tag (news_id, tag_id) VALUES (?, ?)";

    public TagDAOImpl(DataSource dataSource, TagMapper tagMapper) {
        this.dataSource = dataSource;
        this.tagMapper = tagMapper;
    }

    @Override
    public List<Tag> findAll() throws DAOException {
        return findAll(dataSource, SELECT_ALL, tagMapper);
    }

    @Override
    public Tag findOne(Long id) throws DAOException {
        return findOne(id, dataSource, SELECT_TAG_BY_ID, tagMapper);
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return null;
    }

    @Override
    public Boolean delete(Tag entity) throws DAOException {
        return null;
    }

    @Override
    public Tag create(Tag entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TAG, new String[] {"tag_id"})) {
                preparedStatement.setString(1, entity.getTagName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setTagId(resultSet.getLong(1));
                    return entity;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean update(Tag entity) throws DAOException {
        return null;
    }

    @Override
    public Tag findOne(String tagName) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAG_BY_NAME)) {
                preparedStatement.setString(1, tagName);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return tagMapper.takeOne(resultSet);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public Boolean linkNewsTag(Long newsId, Long tagId) throws DAOException {
        return link(newsId, tagId, INSERT_NEWS_TAG);
    }

    private Boolean link(Long firstId, Long secondId, String statement) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                preparedStatement.setLong(1, firstId);
                preparedStatement.setLong(2, secondId);
                return preparedStatement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

}
