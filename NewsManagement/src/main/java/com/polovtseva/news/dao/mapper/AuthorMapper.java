package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class AuthorMapper implements RowMapper<Author> {
    @Override
    public List<Author> takeAll(ResultSet resultSet) throws DAOException {
        try {
            List<Author> authors = new ArrayList<>();
            while (resultSet.next()) {
                authors.add(takeOne(resultSet));
            }
            return authors;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Author takeOne(ResultSet resultSet) throws DAOException {
        try {
            Long authorId = resultSet.getLong("author_id");
            String authorName = resultSet.getString("author_name");
            java.util.Date expired = resultSet.getDate("expired");
            return new Author(authorId, authorName, expired);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
