package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class UserMapper implements RowMapper<User> {
    @Override
    public List<User> takeAll(ResultSet resultSet) throws DAOException {
        List<User> users = new ArrayList<>();
        try {
            while (resultSet.next()) {
                users.add(takeOne(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return users;
    }

    @Override
    public User takeOne(ResultSet resultSet) throws DAOException {
        try {
            Long userId = resultSet.getLong("user_id");
            String userName = resultSet.getString("user_name");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            return new User(userId, userName, login, password);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
