package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Tag;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class TagMapper implements RowMapper<Tag> {
    @Override
    public List<Tag> takeAll(ResultSet resultSet) throws DAOException {
        List<Tag> tags = new ArrayList<>();
        try {
            while (resultSet.next()) {
                tags.add(takeOne(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return tags;
    }

    @Override
    public Tag takeOne(ResultSet resultSet) throws DAOException {
        try {
            Long tagId = resultSet.getLong("tag_id");
            String tagName = resultSet.getString("tag_name");
            return new Tag(tagId, tagName);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
