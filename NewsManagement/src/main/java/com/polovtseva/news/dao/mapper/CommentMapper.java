package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class CommentMapper implements RowMapper<Comment> {
    @Override
    public List<Comment> takeAll(ResultSet resultSet) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        try {
            while (resultSet.next()) {
                comments.add(takeOne(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return comments;
    }

    @Override
    public Comment takeOne(ResultSet resultSet) throws DAOException {
        try {
            Long commentId = resultSet.getLong("comment_id");
            Long newsId = resultSet.getLong("news_id");
            String commentText = resultSet.getString("comment_text");
            Date creationDate = resultSet.getTimestamp("creation_date");
            return new Comment(commentId, commentText, creationDate, newsId);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
