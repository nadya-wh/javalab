package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.AuthorDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.AuthorMapper;
import com.polovtseva.news.entity.Author;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.sql.Date;
import java.util.*;


public class AuthorDAOImpl implements AuthorDAO {

    private DataSource dataSource;
    private AuthorMapper authorMapper;

    private static final String INSERT_AUTHOR = "INSERT INTO AUTHORS (author_id, author_name, expired) " +
            "VALUES (author_id_sequence.nextval, ?, ?)";

    private static final String UPDATE_AUTHOR = "UPDATE AUTHORS SET author_name=?, expired=? WHERE " +
            "author_id=?";

    private static final String SELECT_ONE_AUTHOR = "SELECT author_id, author_name, expired FROM authors WHERE " +
            "author_id=?";

    private static final String SELECT_ALL = "SELECT author_id, author_name, expired FROM authors";

    public AuthorDAOImpl(DataSource dataSource, AuthorMapper authorMapper) {
        this.dataSource = dataSource;
        this.authorMapper = authorMapper;
    }

    @Override
    public List<Author> findAll() throws DAOException {
        return findAll(dataSource, SELECT_ALL, authorMapper);
    }

    @Override
    public Author findOne(Long id) throws DAOException {
        return findOne(id, dataSource, SELECT_ONE_AUTHOR, authorMapper);
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return null;
    }

    @Override
    public Boolean delete(Author entity) throws DAOException {
        return null;
    }

    @Override
    public Author create(Author entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_AUTHOR, new String[]{"author_id"})) {
                preparedStatement.setString(1, entity.getAuthorName());
                preparedStatement.setDate(2, new Date(entity.getExpired().getTime()));
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setAuthorId(resultSet.getLong(1));
                    return entity;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean update(Author entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AUTHOR)) {
                preparedStatement.setString(1, entity.getAuthorName());
                preparedStatement.setDate(2, new Date(entity.getExpired().getTime()));
                preparedStatement.setLong(3, entity.getAuthorId());
                return preparedStatement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

}
