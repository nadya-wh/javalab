package com.polovtseva.news.dao.mapper;

import com.polovtseva.news.dao.exception.DAOException;

import java.sql.ResultSet;
import java.util.List;


public interface RowMapper<T> {

    List<T> takeAll(ResultSet resultSet) throws DAOException;

    T takeOne(ResultSet resultSet) throws DAOException;
}
