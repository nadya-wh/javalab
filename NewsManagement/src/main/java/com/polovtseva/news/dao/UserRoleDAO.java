package com.polovtseva.news.dao;

import com.polovtseva.news.entity.UserRole;

/**
 * User role DAO.
 */
public interface UserRoleDAO extends GenericDAO<UserRole> {
}
