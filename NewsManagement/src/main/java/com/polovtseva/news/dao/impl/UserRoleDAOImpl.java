package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.UserRoleDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.UserRole;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;

/**
 * Created by User on 27.05.2016.
 */
public class UserRoleDAOImpl implements UserRoleDAO {

    private DataSource dataSource;

    public UserRoleDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public List<UserRole> findAll() throws DAOException {
        return null;
    }

    @Override
    public UserRole findOne(Long id) throws DAOException {
        return null;
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return null;
    }

    @Override
    public Boolean delete(UserRole entity) throws DAOException {
        return null;
    }

    @Override
    public UserRole create(UserRole entity) throws DAOException {
        return null;
    }

    @Override
    public Boolean update(UserRole entity) throws DAOException {
        return null;
    }

}
