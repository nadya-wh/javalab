package com.polovtseva.news.dao;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;

import java.util.List;

/**
 * News DAO.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * Finds all news with specified author and tags.
     *
     * @param author the author
     * @param tags the tags
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<News> findAll(Author author, List<Tag> tags) throws DAOException;


    /**
     * Finds all news with specified author.
     *
     * @param author the author
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<News> findAll(Author author) throws DAOException;

    /**
     * Find all news with specified tags.
     *
     * @param tags the tags
     * @return the list
     * @throws DAOException the DAO exception
     */
    List<News> findAll(List<Tag> tags) throws DAOException;

    /**
     * Links news with tag.
     *
     * @param newsId the news id
     * @param tagId the tag id
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean linkNewsTag(Long newsId, Long tagId) throws DAOException;

    /**
     * Links news with author.
     *
     * @param newsId the news id
     * @param authorId the author id
     * @return the boolean
     * @throws DAOException the DAO exception
     */
    Boolean linkNewsAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * Counts news.
     *
     * @return the integer
     * @throws DAOException the DAO exception
     */
    Integer countNews() throws DAOException;
}
