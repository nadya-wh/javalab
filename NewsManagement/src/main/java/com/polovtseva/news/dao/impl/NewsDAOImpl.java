package com.polovtseva.news.dao.impl;

import com.polovtseva.news.dao.NewsDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.dao.mapper.NewsMapper;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 26.05.2016.
 */
public class NewsDAOImpl implements NewsDAO {

    private DataSource dataSource;
    private NewsMapper newsMapper;

    private static final String SELECT_ALL_NEWS = "SELECT n.news_id, title, short_text, full_text, n.creation_date, " +
            "n.modification_date FROM news n LEFT JOIN comments c ON (n.news_id = c.news_id) " +
            "GROUP BY n.news_id, title, short_text, full_text, n.creation_date, n.modification_date " +
            "ORDER BY count(comment_id) DESC";

    private static final String SELECT_ONE_NEWS = "SELECT news_id, title, short_text, full_text, creation_date, " +
            "modification_date FROM news WHERE news_id=?";

    private static final String INSERT_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, " +
            "modification_date) VALUES (NEWS_ID_SEQUENCE.nextval, ?, ?, ?, ?, ?)";

    private static final String DELETE_NEWS = "DELETE FROM news WHERE news_id=?";

    private static final String SELECT_BY_TAGS_AND_AUTHOR = "SELECT news_id, title, short_text, full_text, creation_date, " +
            "modification_date FROM news JOIN news_author USING (news_id) JOIN news_tag USING (news_id) WHERE author_id = ? AND " +
            "tag_id IN (";

    private static final String SELECT_BY_AUTHOR = "SELECT news_id, title, short_text, full_text, creation_date, " +
            "modification_date FROM news JOIN news_author USING (news_id) WHERE author_id=?";

    private static final String UPDATE_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, modification_date=? " +
            "WHERE news_id=?";

    private static final String SELECT_BY_TAGS = "SELECT news_id, title, short_text, full_text, creation_date, " +
            "modification_date FROM news JOIN news_author USING (news_id) JOIN news_tag USING (news_id) WHERE " +
            "tag_id IN (";

    private static final String INSERT_NEWS_TAG = "INSERT INTO news_tag (news_id, tag_id) VALUES (?, ?)";

    private static final String INSERT_NEWS_AUTHOR = "INSERT INTO news_author (news_id, author_id) VALUES (?, ?)";

    private static final String COUNT_NEWS = "SELECT COUNT(*) FROM news";

    public NewsDAOImpl(DataSource dataSource, NewsMapper newsMapper) {
        this.dataSource = dataSource;
        this.newsMapper = newsMapper;
    }

    @Override
    public List<News> findAll() throws DAOException {
        return findAll(dataSource, SELECT_ALL_NEWS, newsMapper);
    }

    @Override
    public News findOne(Long id) throws DAOException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ONE_NEWS)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return newsMapper.takeOne(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) throws DAOException {
        return delete(id, DELETE_NEWS, dataSource);
    }

    @Override
    public Boolean delete(News entity) throws DAOException {
        return delete(entity.getNewsId());
    }

    @Override
    public News create(News entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS, new String[]{"news_id"})) {
                preparedStatement.setString(1, entity.getTitle());
                preparedStatement.setString(2, entity.getShortText());
                preparedStatement.setString(3, entity.getFullText());
                preparedStatement.setTimestamp(4, new Timestamp(entity.getCreationDate().getTime()));
                preparedStatement.setDate(5, new java.sql.Date(entity.getModificationDate().getTime()));
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    entity.setNewsId(resultSet.getLong(1));
                    return entity;
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean update(News entity) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS)) {
                preparedStatement.setString(1, entity.getTitle());
                preparedStatement.setString(2, entity.getShortText());
                preparedStatement.setString(3, entity.getFullText());
                preparedStatement.setDate(4, new java.sql.Date(entity.getModificationDate().getTime()));
                preparedStatement.setLong(5, entity.getNewsId());
                return preparedStatement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findAll(Author author, List<Tag> tags) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String statement = prepareQuery(SELECT_BY_TAGS_AND_AUTHOR, tags.size());

            try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                preparedStatement.setLong(1, author.getAuthorId());
                int index = 2;
                for (Tag tag : tags) {
                    preparedStatement.setLong(index, tag.getTagId());
                    index++;
                }
                ResultSet resultSet = preparedStatement.executeQuery();
                return newsMapper.takeAll(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findAll(Author author) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_AUTHOR)) {
                preparedStatement.setLong(1, author.getAuthorId());
                ResultSet resultSet = preparedStatement.executeQuery();
                return newsMapper.takeAll(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findAll(List<Tag> tags) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String statement = prepareQuery(SELECT_BY_TAGS, tags.size());

            try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                int index = 1;
                for (Tag tag : tags) {
                    preparedStatement.setLong(index, tag.getTagId());
                    index++;
                }
                ResultSet resultSet = preparedStatement.executeQuery();
                return newsMapper.takeAll(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Boolean linkNewsTag(Long newsId, Long tagId) throws DAOException {
        return link(newsId, tagId, INSERT_NEWS_TAG);
    }

    @Override
    public Boolean linkNewsAuthor(Long newsId, Long authorId) throws DAOException {
        return link(newsId, authorId, INSERT_NEWS_AUTHOR);
    }

    @Override
    public Integer countNews() throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(COUNT_NEWS)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    private Boolean link(Long firstId, Long secondId, String statement) throws DAOException {
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
                preparedStatement.setLong(1, firstId);
                preparedStatement.setLong(2, secondId);
                return preparedStatement.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    private String prepareQuery(String query, int count) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            builder.append("?,");
        }
        String statement = query + builder.deleteCharAt(builder.length() - 1).toString() + ")";
        return statement;
    }

}