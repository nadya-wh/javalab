package com.polovtseva.news.service.search;

import com.polovtseva.news.entity.Author;
import com.polovtseva.news.entity.Tag;

import java.util.List;

/**
 * Search criteria class.
 */
public class SearchCriteria {

    private Author author;
    private List<Tag> tags;

    public SearchCriteria(Author author, List<Tag> tags) {
        this.author = author;
        this.tags = tags;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
