package com.polovtseva.news.service;

import com.polovtseva.news.entity.News;
import com.polovtseva.news.service.exception.ServiceException;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public interface NewsManagementService {

    News save(News news) throws ServiceException;

}
