package com.polovtseva.news.service;

import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.exception.ServiceException;

import java.util.List;

/**
 * Tag service interface.
 */
public interface TagService {

    /**
     * Creates the tag.
     *
     * @param tag the tag
     * @return the tag
     * @throws ServiceException the service exception
     */
    Tag createTag(Tag tag) throws ServiceException;


    /**
     * Finds one tag by specified id.
     *
     * @param tagName the tag name
     * @return the tag
     * @throws ServiceException the service layer exception
     */
    Tag findOne(String tagName) throws ServiceException;


    /**
     * Finds all.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Tag> findAll() throws ServiceException;


    void createTags(News news) throws ServiceException;
}
