package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.TagService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of tag service interface.
 */
public class TagServiceImpl implements TagService {

    private TagDAO tagDAO;

    public TagServiceImpl(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Tag createTag(Tag tag) throws ServiceException {
        Tag newTag = findOne(tag.getTagName());
        try {
            if (newTag == null) {
                newTag = tagDAO.create(tag);
            }
            return newTag;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag findOne(String tagName) throws ServiceException {
        try {
            return tagDAO.findOne(tagName);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> findAll() throws ServiceException {
        try {
            return tagDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void createTags(News news) throws ServiceException {
        if (news.getTags() != null && !news.getTags().isEmpty()) {
            for (Tag tag : news.getTags()) {
                Tag foundTag = findOne(tag.getTagName());
                if (foundTag == null) {
                    foundTag = createTag(tag);
                }
                try {
                    if (foundTag != null) {
                        tagDAO.linkNewsTag(news.getNewsId(), tag.getTagId());
                    }
                } catch (DAOException e) {
                    throw new ServiceException(e);
                }
            }
        }
    }

}
