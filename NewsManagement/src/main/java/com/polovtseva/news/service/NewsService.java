package com.polovtseva.news.service;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.search.SearchCriteria;
import com.polovtseva.news.service.exception.ServiceException;

import java.util.List;

/**
 * News service interface.
 */
public interface NewsService {

    /**
     * Adds the news.
     *
     * @param news the news to add.
     * @return the news that was added or null
     * @throws ServiceException the service exception
     */
    News addNews(News news) throws ServiceException;

    /**
     * Edits the news.
     *
     * @param news the news with new values.
     * @return true, if the news was updated successfully; false, otherwise.
     * @throws ServiceException the service exception
     */
    Boolean editNews(News news) throws ServiceException;


    /**
     * Delete news.
     *
     * @param news the news to delete.
     * @return true, if the news was deleted successfully; false, otherwise
     * @throws ServiceException the service exception
     */
    Boolean deleteNews(News news) throws ServiceException;

    /**
     * Reads all news.
     *
     * @return the list of read news.
     * @throws ServiceException the service exception
     */
    List<News> takeAll() throws ServiceException;

    /**
     * Reads one news by id.
     *
     * @param id the id
     * @return the news with specified id or null
     * @throws ServiceException the service exception
     */
    News takeOne(Long id) throws ServiceException;

    /**
     * Takes all news from result set.
     *
     * @param searchCriteria the search criteria
     * @return the list or news.
     * @throws ServiceException the service exception
     */
    List<News> takeAll(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Adds the tag to the news.
     *
     * @param news the news
     * @param tag the tag
     * @return true, if the tag was added successfully; false, otherwise
     * @throws ServiceException the service exception
     */
    Boolean addTag(News news, Tag tag) throws ServiceException;

    void createAuthorLink(News news) throws DAOException;
}
