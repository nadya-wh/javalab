package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.CommentDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.service.CommentService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of comment service.
 */
public class CommentServiceImpl implements CommentService {

    private CommentDAO commentDAO;

    public CommentServiceImpl(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Comment addComment(Comment comment) throws ServiceException {
        try {
            return commentDAO.create(comment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> findAllFor(News news) throws ServiceException {
        try {
            return news != null ? commentDAO.findAllFor(news) : null;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Boolean delete(Long id) throws ServiceException {
        try {
            return id != null ? commentDAO.delete(id) : false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Boolean delete(Comment comment) throws ServiceException {
        try {
            return comment != null ? commentDAO.delete(comment) : false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


}
