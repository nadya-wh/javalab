package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.AuthorDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.Author;
import com.polovtseva.news.service.AuthorService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of author service interface.
 */
public class AuthorServiceImpl implements AuthorService {

    private AuthorDAO authorDAO;

    public AuthorServiceImpl(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Author create(Author author) throws ServiceException {
        try {
            return author != null ? authorDAO.create(author) : null;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author findOne(Long id) throws ServiceException {
        try {
            return id != null ? authorDAO.findOne(id) : null;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Boolean update(Author author) throws ServiceException {
        try {
            return author != null ? authorDAO.update(author) : false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
