package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.NewsManagementService;
import com.polovtseva.news.service.NewsService;
import com.polovtseva.news.service.TagService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Nadzeya_Polautsava on 6/30/2016.
 */
public class NewsManagementServiceImpl implements NewsManagementService {

    private NewsService newsService;
    private TagService tagService;

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public News save(News news) throws ServiceException {
        try {
            if (newsService.addNews(news) != null) {
                tagService.createTags(news);
                newsService.createAuthorLink(news);
                return news;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }


}
