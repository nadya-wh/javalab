package com.polovtseva.news.service;

import com.polovtseva.news.entity.User;
import com.polovtseva.news.service.exception.ServiceException;

/**
 * User service interface.
 */
public interface UserService {

    /**
     * Creates the user.
     *
     * @param user the user
     * @return the user
     * @throws ServiceException the service exception
     */
    User create(User user) throws ServiceException;


    /**
     * Authorisation.
     *
     * @param username the username
     * @param password the password
     * @return the user
     */
    User authorisation(String username, String password);
}
