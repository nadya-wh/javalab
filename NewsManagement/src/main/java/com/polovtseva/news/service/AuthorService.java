package com.polovtseva.news.service;

import com.polovtseva.news.entity.Author;
import com.polovtseva.news.service.exception.ServiceException;

import java.util.List;

/**
 * Author service interface.
 */
public interface AuthorService {

    /**
     * Creates the author.
     *
     * @param author the author
     * @return the author
     * @throws ServiceException the service exception
     */
    Author create(Author author) throws ServiceException;

    /**
     * Finds all authors.
     *
     * @return the list of authors.
     * @throws ServiceException the service exception
     */
    List<Author> findAll() throws ServiceException;

    /**
     * Finds one with specified id.
     *
     * @param id author's id.
     * @return author with specified id or null.
     * @throws ServiceException
     */
    Author findOne(Long id) throws ServiceException;

    /**
     * Updates author info.
     *
     * @param author with updated information.
     * @return author with updated information.
     * @throws ServiceException service layer exception.
     */
    Boolean update(Author author) throws ServiceException;


}
