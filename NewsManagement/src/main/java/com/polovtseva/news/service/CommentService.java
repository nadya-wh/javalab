package com.polovtseva.news.service;

import com.polovtseva.news.entity.Comment;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.service.exception.ServiceException;

import java.util.List;

/**
 * Comment service.
 */
public interface CommentService {

    /**
     * Adds the comment.
     *
     * @param comment the comment
     * @return the comment
     * @throws ServiceException the service exception
     */
    Comment addComment(Comment comment) throws ServiceException;

    /**
     * Finds all for news.
     *
     * @param news the news
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Comment> findAllFor(News news) throws ServiceException;

    /**
     * Delete.
     *
     * @param id the id
     * @return the boolean
     * @throws ServiceException the service exception
     */
    Boolean delete(Long id) throws ServiceException;

    /**
     * Delete.
     *
     * @param comment the comment
     * @return the boolean
     * @throws ServiceException the service exception
     */
    Boolean delete(Comment comment) throws ServiceException;
}
