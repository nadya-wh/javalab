package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.UserDAO;
import com.polovtseva.news.dao.UserRoleDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.User;
import com.polovtseva.news.service.UserService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of user service interface.
 */
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;
    private UserRoleDAO userRoleDAO;

    public UserServiceImpl(UserDAO userDAO, UserRoleDAO userRoleDAO) {
        this.userDAO = userDAO;
        this.userRoleDAO = userRoleDAO;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public User create(User user) throws ServiceException {
        try {
            return userDAO.create(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public User authorisation(String username, String password) {

        return null;
    }


}
