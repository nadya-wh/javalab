package com.polovtseva.news.service.impl;

import com.polovtseva.news.dao.NewsDAO;
import com.polovtseva.news.dao.TagDAO;
import com.polovtseva.news.dao.exception.DAOException;
import com.polovtseva.news.entity.News;
import com.polovtseva.news.entity.Tag;
import com.polovtseva.news.service.search.SearchCriteria;
import com.polovtseva.news.service.NewsService;
import com.polovtseva.news.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of news service interface.
 */
public class NewsServiceImpl implements NewsService {

    private NewsDAO newsDAO;
    private TagDAO tagDAO;

    public NewsServiceImpl(NewsDAO newsDAO, TagDAO tagDAO) {
        this.newsDAO = newsDAO;
        this.tagDAO = tagDAO;
    }

//    @Override
//    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
//    public News addNews(News news) throws ServiceException {
//        try {
//            if (newsDAO.create(news) != null) {
//                createTags(news);
//                createAuthorLink(news);
//                return news;
//            }
//        } catch (DAOException e) {
//            throw new ServiceException(e);
//         }
//        return null;
//    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public News addNews(News news) throws ServiceException {
        try {
            return newsDAO.create(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }




    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Boolean editNews(News news) throws ServiceException {
        try {
            return newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public Boolean deleteNews(News news) throws ServiceException {
        try {
            return newsDAO.delete(news);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> takeAll() throws ServiceException {
        try {
            return newsDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News takeOne(Long id) throws ServiceException {
        try {
            return newsDAO.findOne(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> takeAll(SearchCriteria searchCriteria) throws ServiceException {
        try {
            if (searchCriteria.getAuthor() != null && !searchCriteria.getTags().isEmpty()) {
                return newsDAO.findAll(searchCriteria.getAuthor(), searchCriteria.getTags());
            } else if (searchCriteria.getAuthor() != null) {
                return newsDAO.findAll(searchCriteria.getAuthor());
            } else if (!searchCriteria.getTags().isEmpty()) {
                return newsDAO.findAll(searchCriteria.getTags());
            }
            return null;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Boolean addTag(News news, Tag tag) throws ServiceException {
        try {
            Tag createdTag = tagDAO.create(tag);
            if (createdTag != null) {
                return newsDAO.linkNewsTag(news.getNewsId(), tag.getTagId());
            }
            return new Boolean(false);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    public void createAuthorLink(News news) throws DAOException {
        if (news.getAuthor() != null) {
            newsDAO.linkNewsAuthor(news.getNewsId(), news.getAuthor().getAuthorId());
        }
    }



}
